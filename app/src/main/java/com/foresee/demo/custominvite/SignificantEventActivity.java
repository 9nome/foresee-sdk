package com.foresee.demo.custominvite;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.foresee.sdk.ForeSee;
import com.foresee.sdk.common.configuration.MeasureConfiguration;
import com.foresee.sdk.cxMeasure.tracker.listeners.DefaultInviteListener;

public class SignificantEventActivity extends AppCompatActivity {

    private static final String TAG = "SignificantEvents";
    /* Captain's log
    This activity's intent is to test significant events

    This worked right after I uninstalled reinstalled the app

    I wish the log would tell me eligibility failed because of the signigicant event,
    this seems very valuable, I made sure debug logging was enabled

    it onyl says eligibility failed

    if sig event is over set amount, it will still trigger an invite, but it may fail due to not being far enough from decline date.

    PENDING_REINVITE_AFTER_DECLINE if I see this it failed because it was too close to date after decline

    sdk will auto reset state after repeat days period

    reset will reset significant events, and it looks like repeatDays

    anythin inside one of the measures object /s in config file must be true before itll fire

in this activity, it says survey will be sent to null, but i get the survey ok

     */
    int significantEventTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_invite3);
        significantEventTest = 0;
        //If coming into app second time without resetting, invite can pop immediately.
        ForeSee.resetState();


        //Set Listeners (optional)
        ForeSee.setInviteListener(new DefaultInviteListener() {
            @Override
            public void onInvitePresented(MeasureConfiguration measureConfiguration) {
                Log.d(TAG, "onInvitePresented");
                Toast.makeText(getApplicationContext(), "Invite presented", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInviteCompleteWithAccept() {
                Log.d(TAG, "onInviteAccepted");
                Toast.makeText(getApplicationContext(), "A survey will be sent to " + ForeSee.getContactDetails(), Toast.LENGTH_SHORT).show();

                //Reset
                ForeSee.resetState();
            }

            @Override
            public void onInviteCompleteWithDecline() {
                Log.d(TAG, "onInviteDeclined");
                Toast.makeText(getApplicationContext(), "Invitation declined by user", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSurveyPresented() {
                Log.d(TAG, "onSurveyPresented");
                Toast.makeText(getApplicationContext(), "Survey presented", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSurveyCompleted() {
                Log.d(TAG, "onSurveyCompleted");
                Toast.makeText(getApplicationContext(), "Survey completed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSurveyCancelledByUser() {
                Log.d(TAG, "onSurveyCancelled");
                Toast.makeText(getApplicationContext(), "Survey cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSurveyCancelledWithNetworkError() {
                Log.d(TAG, "onSurveyCancelledWithNetworkError");
                Toast.makeText(getApplicationContext(), "Survey cancelled with network error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInviteCancelledWithNetworkError() {
                Log.d(TAG, "onInviteCancelledWithNetworkError");
                Toast.makeText(getApplicationContext(), "Invitation cancelled with network error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInviteNotShownWithNetworkError(MeasureConfiguration measureConfiguration) {
                Log.d(TAG, "onInviteNotShownWithNetworkError");
                Toast.makeText(getApplicationContext(), "Invitation not shown with network error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInviteNotShownWithEligibilityFailed(MeasureConfiguration measureConfiguration) {
                Log.d(TAG, "onInviteNotShownWithEligibilityFailed");
                Toast.makeText(getApplicationContext(), "Invitation not shown with eligibility failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInviteNotShownWithSamplingFailed(MeasureConfiguration measureConfiguration) {
                Log.d(TAG, "onInviteNotShownWithSamplingFailed");
                Toast.makeText(getApplicationContext(), "Invitation not shown with sampling failed", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void launchDefaultInvite(View view)
    {
        // Launch an invite as a demo
        //ForeSee.showInviteForSurveyID("app_test_1");

        //check if elgigible doesn't have value here(while i check in the incrementSE method) because will always be inelgigible after SE is >3, and you cant fire survey until SE threshold is reached.
        ForeSee.checkIfEligibleForSurvey();
    }

    public void incrementSignificantEvent(View view)
    {

        significantEventTest+=1;
        Log.d("gnome","***incrementing signfiicant events***");
        Toast.makeText(getApplicationContext(), "Significant Event Incremented: "+significantEventTest, Toast.LENGTH_SHORT).show();
        ForeSee.incrementSignificantEventCountWithKey("sigEventTest");
        ForeSee.checkIfEligibleForSurvey();
    }

    public void resetState(View v){
        Log.d("gnome","reset");
        Toast.makeText(getApplicationContext(), "Reset state", Toast.LENGTH_SHORT).show();
        ForeSee.resetState();
        significantEventTest = 0;
    }

}
